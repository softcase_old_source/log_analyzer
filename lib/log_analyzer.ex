defmodule LogAnalyzer do
  defmodule Session do
    defstruct number: 0, bytes: []
  end

  def analyze do
    File.stream!("in.log")
    |> Stream.map(&String.split(&1, " "))
    |> Enum.reduce(%{x: [], ys: [], startTime: 0, max: 0, max_sess: []},
            fn(x, acc)->
              case  Map.get(acc, :x) do
                [] -> acc=%{acc | x: Map.get(acc, :x) ++ [0]}
                      startTime=(Enum.at(x, 0) |> String.slice(0..7) |> Time.from_iso8601! |> Time.to_erl |> :calendar.time_to_seconds)
                      acc=%{acc | startTime: startTime}

                 _ -> t=(Enum.at(x, 0) |> String.slice(0..7) |> Time.from_iso8601! |> Time.to_erl |> :calendar.time_to_seconds)
                      acc=%{acc | x: Map.get(acc, :x) ++ [t-Map.get(acc, :startTime)]}
              end


              number=Enum.at(x, 2)<>":"<>(String.split(Enum.at(x,3), ":") |> Enum.at(0) |> String.slice(1..20))
              bytes=(case Enum.at(x,4) do
                "OnRead" -> "-"<>Enum.at(x, 6)
                "OnWrite" -> Enum.at(x, 6)
                "OnConnect" -> "8000"
                "OnDisconnect" -> "-8000"
                "OnError" -> "-15000"
              end) |> String.to_integer


              case Map.get(acc, :ys) do
                [] -> acc=%{acc | ys: [%Session{number: number, bytes: [bytes]}]}
                      if bytes>0, do: acc=Map.merge(acc, %{max: 1, max_sess: [number]})
                 _ ->

                    case Enum.find(Map.get(acc, :ys), fn (%Session{number: num, bytes: b})-> num==number end) do
                      %Session{number: num, bytes: b} -> acc=%{acc | ys: List.delete(Map.get(acc, :ys), %Session{number: num, bytes: b})
                                                                     |> List.insert_at(0, %Session{number: num, bytes: b++[bytes]})}
                                                         if bytes<=-8000, do: acc=Map.merge(acc, %{max: Map.get(acc, :max)-1, max_sess: Map.get(acc, :max_sess)--[num]})

                      _ -> acc=%{acc | ys: Map.get(acc, :ys) ++ [%Session{number: number, bytes: Enum.reduce(Map.get(acc, :x), [], fn (x, acc) -> [0 | acc] end) ++ [bytes]}]}
                           if bytes>0, do: acc=Map.merge(acc, %{max: Map.get(acc, :max)+1, max_sess: Map.get(acc, :max_sess)++[number]})
                    end
              end
              acc
            end)
    |> total
    # |> together
    # |> plot

    # |> Stream.into(File.stream!("output.txt", [:write, :utf8]))
    # |> Stream.run
  end

  def total(%{x: x, ys: y, max: max, max_sess: sess}) do
    IO.puts "max=#{max}"
    IO.inspect sess

    Enum.each(y, fn (%Session{number: num, bytes: b})->
                    sum=Enum.reduce(b, 0, fn (x, acc) -> if (x>0 and x<=4000), do: acc+x, else: acc end)
                    IO.puts "#{num}=#{sum}"
                 end)
    %{x: x, ys: y}
  end

  def plot(%{x: x, ys: y}) do
    IO.puts "x=#{Enum.count(x)} ys=#{Enum.count(y)}"

    plot=Explot.new
    Explot.ylabel(plot, "bytes")
    Explot.xlabel(plot, "time")
    Explot.title(plot, "session")
    Enum.each(y, fn (%Session{number: number, bytes: bytes}) -> Explot.add_list(plot, bytes, number) end)
    Explot.x_axis_labels(plot, x)
    Explot.show(plot)
  end
end
